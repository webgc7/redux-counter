import React, { Component } from "react";
import PropTypes from "prop-types";

import Header from "./Header";
import Footer from "./Footer";

class Counter extends Component {
   constructor(props) {
      super(props);
      this.startAsync = this.startAsync.bind(this);
      this.stopAsync = this.stopAsync.bind(this);
      this.stateInterval = () => (this.stateInterval = setInterval(this.props.onIncrement, 1000));
   }

   startAsync() {
      if (typeof this.stateInterval === "function") {
         this.stateInterval();
      }
   }

   stopAsync() {
      if (!isNaN(this.stateInterval)) {
         clearInterval(this.stateInterval);
         this.stateInterval = () => (this.stateInterval = setInterval(this.props.onIncrement, 1000));
      }
   }

   createButton(action, body, btnStyle) {
      // console.log(action);
      return (
         <button type="button" onClick={action} className={` btn btn-${btnStyle} shadow-sm`}>
            {body}
         </button>
      );
   }

   render() {
      const { value, onIncrement, onDecrement } = this.props;
      return (
         <>
            <Header />
            <section className="card position-absolute top-50 start-50 translate-middle text-center w-75 bg-light shadow">
               <div className="card-body">
                  <p className="card-text fs-3 p-2 bg-white rounded shadow-sm">{value}</p>

                  <div className="row row-cols-2">
                     <div className="col d-grid gap-3">
                        {this.createButton(onDecrement, "-", "outline-danger")}
                        {this.createButton(this.stopAsync, "STOP", "danger")}
                     </div>
                     <div className="col d-grid  gap-3 ">
                        {this.createButton(onIncrement, "+", "outline-success")}
                        {this.createButton(this.startAsync, "START", "success")}
                     </div>
                  </div>
               </div>
            </section>
            <Footer />
         </>
      );
   }
}

Counter.propTypes = {
   value: PropTypes.number.isRequired,
   onIncrement: PropTypes.func.isRequired,
   onDecrement: PropTypes.func.isRequired,
};

export default Counter;
